<?php

namespace App\Http\Controllers;

use App\Http\Resources\NoteResource;
use App\Models\Note;
use Illuminate\Http\Request;

use function PHPSTORM_META\map;

class NoteController extends Controller
{
    public function show(){
        try {
            $notes = Note::get();

            return response()->json([
                'data' => NoteResource::collection($notes),
                'status' => 200,
                'message' => 'success get all notes'
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 200,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function store(Request $request) {
        try {

            $note = Note::create([
                'title' => $request->title,
                'text' => $request->text
            ]);

            return response()->json([
                'data' => new NoteResource($note),
                'status' => 200,
                'message' => 'success create note'
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 200,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function update(Request $request) {
        try {

            $id = $request->id;

            if( !$id ) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Note id is null'
                ],500);
            }

            $note = Note::find($id);
            if( !$note ) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Note not found'
                ],500);
            }

            $note->title = $request->title;
            $note->text = $request->text;
            $note->save();

            return response()->json([
                'data' => new NoteResource($note),
                'status' => 200,
                'message' => 'success create note'
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 200,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function destroy($id){
        try {

            // $id = $request->id;

            if( !$id ) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Note id is null'
                ],500);
            }

            $note = Note::find($id);

            if( !$note ) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Note not found'
                ],500);
            }

            $note->delete();

            return response()->json([
                'status' => 200,
                'message' => 'success destroy notes'
            ],200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 200,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}

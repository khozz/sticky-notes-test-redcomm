<?php

use App\Http\Controllers\NoteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/notes', [NoteController::class, 'show']);
Route::post('/', [NoteController::class, 'store']);
Route::post('/notes/update', [NoteController::class, 'update']);
Route::delete('/notes/destroy/{id}', [NoteController::class, 'destroy']);

